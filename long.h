///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.h
/// @version 1.0
///
/// Print the characteristics of the "long" and "unsigned long" datatypes.
///
/// @author Christian Yang <ccyang3@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   25/01/21
///////////////////////////////////////////////////////////////////////////////

extern void doLong();            /// Print the characteristics of the "long" datatype
extern void flowLong();          /// Print the overflow/underflow characteristics of the "long" datatype

extern void doSignedLong();      /// Print the characteristics of the "signed long" datatype
extern void flowSignedLong();    /// Print the overflow/underflow characteristics of the "signed long" datatype

extern void doUnsignedLong();    /// Print the characteristics of the "unsigned long" datatype
extern void flowUnsignedLong();  /// Print the overflow/underflow characteristics of the "unsigned long" datatype

